<!DOCTYPE html>
<html>
	<head>
		<title>Share score form</title>
		<style type="text/css">
		.form-field {
			margin: 5px 0;
		}
		.form-field label {
			display: inline-block;
			vertical-align: middle;
			width: 100px;
		}
		</style>
		<script type="text/javascript" src="/js/leader-table.js"></script>
		<script type="text/javascript">
			function shareScore(htmlForm)
			{
				var data = {
					playerName: htmlForm.playerName.value,
					score: htmlForm.score.value,
					date: htmlForm.date.value
				};

				ajax(
					"/api/share-score.php?data=" + JSON.stringify(data),
					"GET",
					function() {
						var msg = document.getElementsByClassName("message")[0];
						msg.innerText = this.message;

						htmlForm.playerName.value = "";
						htmlForm.score.value = "";
						htmlForm.date.value = "";
					});

				return false;
			}
		</script>
	</head>
	<body>
		<h1>Share Your Score!</h1>
		<form onsubmit="return shareScore(this);">
			<div class="form-field">
				<label>Имя</label>
				<input type="text" name="playerName">
			</div>
			<div class="form-field">
				<label>Очки</label>
				<input type="number" name="score">
			</div>
			<div class="form-field">
				<label>Дата</label>
				<input type="date" name="date">
			</div>
			<div class="form-field">
				<label class="message"></label>
			</div>
			<div>
				<input type="submit" value="Отправить">
			</div>
		</form>
	</body>
</html>