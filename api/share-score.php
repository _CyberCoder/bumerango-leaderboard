<?php
	header("Content-Type: application/json");
	require_once(__DIR__."/../lib/check-db.php");

	if (!isset($_GET["data"]))
	{
		$responce = array(
			"status" => "error",
			"message" => "Bad request");

		echo json_encode($responce);
		die();
	}

	$json = json_decode($_GET["data"]);

	if (is_object($json) && property_exists($json, "playerName") && property_exists($json, "score") && property_exists($json, "date"))
	{
		$db = get_database();
		$query = "INSERT INTO leaders (player_name, score, play_date) VALUES ('" . $json->playerName . "', " . $json->score . ", '" . $json->date . "');";
		$result = $db->exec($query);

		if ($result)
		{
			$responce = array(
				"status" => "succsess",
				"message" => "OK");

			echo json_encode($responce);
		}
		else
		{
			$responce = array(
				"status" => "error",
				"message" => "Error inserting data to database");

			echo json_encode($responce);
		}

		$db->close();
		unset($db);
	}
	else
	{
		$responce = array(
			"status" => "error",
			"message" => "Not all fields has value");

		echo json_encode($responce);
	}
?>