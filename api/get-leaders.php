<?php
	header("Content-Type: application/json");
	require_once(__DIR__."/../lib/check-db.php");

	function get_leaders($date_start, $date_end)
	{
		$db = get_database();
		$query = "SELECT player_name, score FROM leaders";

		if ($date_start != null && $date_end != null)
		{
			$query .= " WHERE play_date BETWEEN '" . $date_start . "' AND '" . $date_end . "'";
		}

		$query .= " ORDER BY score DESC;";		
		$result = $db->query($query);

		$result_array = array();
		$rank = 1;

		while ($row = $result->fetchArray())
		{
			$row["rank"] = $rank++;
			array_push($result_array, $row);
		}

		$db->close();
		unset($db);

		return $result_array;
	}

	$periods = array("daily", "weekly", "monthly", "alltime");

	if (!isset($_GET["period"]))
	{
		$period = "daily";
	}
	else
	{
		$period = $_GET["period"];
		if (!in_array($period, $periods)) $period = "daily";
	}
	
	date_default_timezone_set("UTC");
	$current_date = date("d.m.Y");

	switch ($period)
	{
		case "daily":
			$result = get_leaders($current_date, $current_date);
			break;
		
		case "weekly":
			$week_start = date('d.m.Y', strtotime('this week last monday'));
			$week_end = date('d.m.Y', strtotime('this week next sunday'));
			$result = get_leaders($week_start, $week_end);
			break;

		case "monthly":
			$month_start = date('01.m.Y');
			$month_end = date('t.m.Y');
			$result = get_leaders($month_start, $month_end);
			break;

		case "alltime":
			$result = get_leaders(null, null);
			break;
	}

	echo json_encode($result);
?>