<!DOCTYPE html>
<html>
	<head>
		<title>BumeranGO Leaderboard</title>
		<style type="text/css">
		.score-form,
		.leaderboard {
			position: absolute;
			top: 0;
			display: inline-block;
			box-shadow: 0 0 10px 2px rgba(0,0,0,0.5);
			border: 0;
		}

		.score-form {
			left: 0;
			width: 300px;
			height: 230px;
		}

		.leaderboard {
			right: 0;
			width: 500px;
			height: 500px;
		}
		</style>
	</head>
	<body>
		<iframe class="score-form" src="score-form.php"></iframe>
		<iframe class="leaderboard" src="leaderboard.php"></iframe>
	</body>
</html>