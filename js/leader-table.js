function table(tableBodyElement)
{
	this.tableBodyElement = tableBodyElement;
	this.render = function (items)
	{
		var tbody = document.createElement("tbody");

		for (var i = 0; i < items.length; i++)
		{
			var item = items[i];
			var tr = document.createElement("tr");

			var td = document.createElement("td");
			var text = document.createTextNode(item.rank);
			td.appendChild(text);
			tr.appendChild(td);

			td = document.createElement("td");
			text = document.createTextNode(item.player_name);
			td.appendChild(text);
			tr.appendChild(td);

			td = document.createElement("td");
			text = document.createTextNode(item.score);
			td.appendChild(text);
			tr.appendChild(td);

			tbody.appendChild(tr);
		}

		var parent = this.tableBodyElement.parentNode;
		parent.replaceChild(tbody, this.tableBodyElement);
		this.tableBodyElement = tbody;
	}
}

function ajax(url, method, success)
{
    var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4)
        {
           if (xmlhttp.status == 200)
           {
              success.apply(JSON.parse(xmlhttp.responseText));
           }
           else if(xmlhttp.status == 400)
           {
              console.log('ajax: There was an error 400');
           }
           else
           {
              console.log('ajax: something else other than 200 was returned');
           }

           xmlhttp = null;
        }
    }

    xmlhttp.open(method, url, true);
    xmlhttp.send();
}

function leaderUpdater(tableRenderer, period)
{
	ajax(
		"/api/get-leaders.php?period=" + (period || "daily"),
		"GET",
		function() {
			tableRenderer.render(this);
		});
}

var domReady = (function ()
{
	var arrDomReadyCallBacks = [];
  	
  	function excuteDomReadyCallBacks()
  	{
  		for (var i = 0; i < arrDomReadyCallBacks.length; i++)
  		{
  			arrDomReadyCallBacks[i]();
  		}

  		arrDomReadyCallBacks = [];
  	}

  	return function (callback)
  	{
  		arrDomReadyCallBacks.push(callback);
    
	    if (document.addEventListener)
	    {
	        document.addEventListener('DOMContentLoaded', excuteDomReadyCallBacks, false);
	    }
	    
	    if (/KHTML|WebKit|iCab/i.test(navigator.userAgent))
	    {
	        browserTypeSet = true ;
	     
	        var DOMLoadTimer = setInterval(function ()
	        {
	            if (/loaded|complete/i.test(document.readyState))
	            {
	                //callback();
	                excuteDomReadyCallBacks();
	                clearInterval(DOMLoadTimer);
	            }
	        }, 10);
	    }

 	   window.onload = excuteDomReadyCallBacks;
	}
})();