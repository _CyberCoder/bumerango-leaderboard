<!DOCTYPE html>
<html>
	<head>
		<title>Leaderboard</title>
		<style type="text/css">
		html,
		body,
		.board ul {
			margin: 0;
			padding: 0;
		}

		.board ul {
			list-style: none;
			width: 100%;
			display: table;
		}

		.board ul li {
			display: table-cell;
			width: auto;
			height: 50px;
  			text-align: center;
  			vertical-align: middle;
  			border: 1px solid gray;
  			font-weight: bold;
  			cursor: pointer;
		}

		.board ul li.selected {
			background: #090;
			color: #fff;
		}

		.board ul li:hover {
			background: #0a0;
			color: #fff;
		}

		table.leaders {
			width: 100%;
			table-layout: fixed;
			border-collapse: collapse;
		}

		table.leaders thead tr {
			background: #f50;
			border-radius: 5px 0;
		}

		table.leaders thead th {
			border: 1px solid #f70;
			padding: 5px;
			color: #fff;
		}

		table.leaders tbody tr:hover {
			background: #ddd;
			cursor: pointer;
		}

		table.leaders tbody td {
			padding: 5px;
			text-align: center;			
		}
		</style>
		<script type="text/javascript" src="/js/leader-table.js"></script>
		<script type="text/javascript">
			var period = "daily";
			var tableRenderer = null;

			domReady(function() {
				var tableElement = document.getElementsByClassName("leaders")[0];
				tableRenderer = new table(tableElement.getElementsByTagName("tbody")[0]);

				setInterval(function() {
					leaderUpdater(tableRenderer, period);
				}, 10000);

				leaderUpdater(tableRenderer);
			});

			function periodChange(el, newPeriod)
			{
				var selectedLi = document.getElementsByClassName("selected")[0];
				selectedLi.setAttribute("class", "");
				el.setAttribute("class", "selected");
				period = newPeriod;

				leaderUpdater(tableRenderer, period);
			}
		</script>
	</head>
	<body>
		<div class="board">
			<ul>
				<li class="selected" onclick="periodChange(this, 'daily');">Daily</li>
				<li onclick="periodChange(this, 'weekly');">Weekly</li>
				<li onclick="periodChange(this, 'monthly');">Monthly</li>
				<li onclick="periodChange(this, 'alltime');">All Time</li>
			</ul>
			<div>				
				<table class="leaders">
					<thead>
						<tr>
							<th>Rank</th>
							<th>Name</th>
							<th>Score</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>John</td>
							<td>11000</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Bobby</td>
							<td>10235</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Tom</td>
							<td>7210</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>