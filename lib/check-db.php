<?php
	function get_database()
	{
		return new SQLite3(__DIR__."/../data/leaderboard.db3");
	}

	$db = get_database();
	
	$query = "SELECT CASE WHEN tbl_name = 'leaders' THEN 1 ELSE 0 END FROM sqlite_master WHERE tbl_name = 'leaders' AND type = 'table';";
 	$result = $db->query($query);
	$exist = $result->fetchArray();
	
	if (!$exist)
	{
		$query = "CREATE TABLE leaders (player_name TEXT, score INTEGER, play_date DATE);";
		$db->exec($query);

		for ($i = 0; $i < 50; $i++)
		{ 
			$query = "INSERT INTO leaders (player_name, score, play_date) VALUES ('Player " . $i . "', " . ($i * 205 + 256) . ", '" . date("d.m.Y - " . ($i * 5) . " days") . "');";
			$db->exec($query);
		}

		$query = "INSERT INTO leaders (player_name, score, play_date) VALUES ('Player 50', 10506, '" . date("d.m.Y") . "');";
		$db->exec($query);
	}

	$db->close();
	unset($db);
?>